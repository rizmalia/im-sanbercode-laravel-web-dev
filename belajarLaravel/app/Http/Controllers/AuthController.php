<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        $nmdepan = $request['fname'];
        $nmbelakang = $request['lname'];

        return view('welcome', ['nmdepan' => $nmdepan, 'nmbelakang' => $nmbelakang]);
    }
}
