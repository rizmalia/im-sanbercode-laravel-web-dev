@extends('layouts.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('sub-title')
    Detail Cast
@endsection

@section('content')
<a href="/cast" class="btn btn-primary btn-sm">Kembali</a>

<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

@endsection