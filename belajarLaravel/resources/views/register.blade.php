<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h4>Sign Up Form</h4>

    <form action="/welcome" method="post">
        @csrf
        <label>First Name: </label> <br>
        <input type="text" name='fname'><br><br>

        <label>Last Name: </label><br>
        <input type="text" name='lname'><br><br>

        <label>Gender: </label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br><br>
        <input type="radio" name="gender">Other<br><br>

        <label>Nationality: </label><br>
        <select name="nationality">
            <option value="">Indonesian</option>
            <option value="">Singaporean</option>
            <option value="">Malaysian</option>
            <option value="">Australian</option>
        </select><br><br>

        <label>Language Spoken: </label><br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other <br>

        <label>Bio: </label><br>
        <textarea cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>