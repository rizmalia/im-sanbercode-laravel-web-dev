<?php

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("Shaun");

echo $sheep->nama."<br>";
echo $sheep->legs."<br>";
echo $sheep->cold_blooded."<br><br>";

$kodok = new Frog("buduk");
echo $kodok->nama."<br>";
echo $kodok->legs."<br>";
echo $kodok->cold_blooded."<br";
echo $kodok->jump();

$sungokong = new Ape('kera sakti');
echo $sungokong->nama."<br>";
echo $sungokong->legs."<br>";
echo $sungokong->cold_blooded."<br";
echo $sungokong->yell();




?>